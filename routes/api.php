<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register',['as'=>'register','uses'=>'RegisterController@index']);

Route::put('/comment',['uses'=>'CommentController@store'])->middleware('auth:api');
Route::get('/comment/{blog}',['uses'=>'CommentController@index']);

Route::get('/blogs/{blog}',['uses'=>'BlogController@show']);

Route::get('/all_blogs',['uses'=>'BlogController@all']);

Route::group(['prefix'=>'/blogs','middleware'=>['auth:api']],function(){

    Route::get('/',['as'=>'blogs','uses'=>'BlogController@index']);

    Route::put('/',['as'=>'blogs.store','uses'=>'BlogController@store']);
    Route::post('/{blog}',['as'=>'blogs.update','uses'=>'BlogController@update']);
    Route::get('/delete/{id}',['as'=>'blogs.destory','uses'=>'BlogController@destory']);

});