import VueRouter from 'vue-router';

let routes = [
    {
        path : '/',
        component : require('./pages/welcome')
    },
    {
        path : '/all-blogs',
        component : require('./pages/blogs')
    },
    {
        path : '/login',
        component : require('./components/auth/login'),
        meta : {
            forVisitors : true
        }
    },
    {
        path : '/sign-up',
        component : require('./components/auth/registration.vue'),
        meta : {
            forVisitors : true
        }
    },
    {
        path : '/blogs',
        component : require('./components/blog/blogs.vue'),
        meta : {
            forAuth : true
        }
    },
    {
        path : '/blog/add',
        component : require('./components/blog/add.vue'),
        meta : {
            forAuth : true
        }
    },
    {
        path : '/blog/:id/:slug',
        name: 'blog',
        component : require('./components/blog/blog.vue')
    },
    {
        path : '/logout',
        component : require('./components/auth/logout.vue'),
        meta : {
            forAuth : true
        }
    }
];

export default new VueRouter({
    routes,
    linkActiveClass:'active'
})