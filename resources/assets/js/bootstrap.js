

import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VeeValidate from 'vee-validate';

window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(VeeValidate);

window.axios = axios;

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

