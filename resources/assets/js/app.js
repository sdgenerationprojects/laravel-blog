
import './bootstrap';


import router from './routes.js';
import  navigation from './components/elements/nav.vue';
import Auth from './utility/Auth.js';


import swal from 'sweetalert';

router.beforeEach(
    (to,form,next) =>{
        if(to.matched.some(record => record.meta.forVisitors)){
            if(Auth.isAuthanticated()){
                next({
                    path:'/blogs'
                })
            }else{
                next()
            }
        }else{
            if(to.matched.some(record => record.meta.forAuth)){
                if(!Auth.isAuthanticated()){
                    next({
                        path:'/login'
                    })
                }else{
                    next()
                }
            }else{
                next()
            }
        }

    }
)

new Vue({
    el: '#app',
    router,
    components: {
        navigation
    },
});
