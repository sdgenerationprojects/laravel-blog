class Blog{
    static store(access_key,data){
        return axios.put('/api/blogs',data,{headers : { Authorization : 'Bearer '+ access_key }});
    }
    static get(access_key){
        return axios.get('/api/blogs',{headers : { Authorization : 'Bearer '+ access_key }});
    }
    static gets(){
        return axios.get('/api/all_blogs');
    }
    static show(access_key,id){
        return axios.get('/api/blogs/'+id,{headers : { Authorization : 'Bearer '+ access_key }});
    }

    static delete(access_key,id){
        return axios.get('/api/blogs/delete/'+id,{headers : { Authorization : 'Bearer '+ access_key }});
    }
}
export default Blog;