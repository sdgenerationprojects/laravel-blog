
let authanticatedUser = {};

class Auth {

   static setToken(token , expiration) {
        localStorage.setItem('token',token);
        localStorage.setItem('expiration',expiration);
    }

    static getToken () {
        var token = localStorage.getItem('token');
        var expiration = localStorage.getItem('expiration');

        if(!token || !expiration){
            return null;
        }

        if(Date.now() > parseInt(expiration)){
            this.destoryToken();
            return null;
        }else{
            return token;
        }

    }

    static destoryToken(){
        localStorage.removeItem('token');
        localStorage.removeItem('expiration');
    }

    static isAuthanticated (){
        if(this.getToken()){
            return true;
        }else{
            return false;
        }
    }

    static setAuthanticatedUser(data){
        authanticatedUser = data;
    }

    static getAuthanticatedUser(){
        return authanticatedUser;
    }

}

export  default  Auth;
