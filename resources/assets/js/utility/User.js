class User{
    static register(data){
        return axios.post('/api/register',data);
    }
    static login(data){
        data.client_id = 2;
        data.client_secret = '181z6Nd8gHJBddODPpshNLrmnQ2D4MJ6S2re3Qoy';
        data.grant_type = 'password';
        data.scope = '*';

        return axios.post('/oauth/token',data);
    }

    static get(data){

        return axios.get('/api/user',{headers : { Authorization : 'Bearer '+ data } });

    }
}

export  default  User;