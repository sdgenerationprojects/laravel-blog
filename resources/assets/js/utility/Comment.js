class Comment{

    static store(access_key,data){
        return axios.put('/api/comment',data,{headers : { Authorization : 'Bearer '+ access_key }});
    }

    static get(blog_id){
        return axios.get('/api/comment/'+blog_id);
    }

}
export default Comment;