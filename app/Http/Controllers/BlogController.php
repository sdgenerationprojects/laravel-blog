<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    public  function index(Request $request){
        $user = $request->user();
        $blogs = Blog::with('author')->where('author_id','=',$user->id)->latest()->get();
        return response($blogs);
    }

    public  function all(){
        $blogs = Blog::with('author')->latest()->get();
        return response($blogs,200);
    }

    public  function store(Request $request){

        $blog = new Blog;
        $blog->author_id = $request->author_id;
        $blog->title = $request->title;
        $blog->slug = str_slug($request->title);
        $blog->detail = $request->detail;

        $blog->save();

        return response($blog,200);
    }

    public  function show($blog){
        $blog = Blog::with('author')->where('id','=',$blog)->first();
        return response($blog);
    }

    public function update(Request $request,Blog $blog){
        $blog->title = $request->get('title',$blog->title);
        if($request->has('title')){
            $blog->slug = str_slug($request->title);
        }
        $blog->detail = $request->get('detail',$blog->detail);

        $blog->save();
        return $blog;
    }

    public  function destory($id){
        $blog = Blog::where('id','=',$id)->first();
        $blog->delete();
        return response(null,200);
    }

}
