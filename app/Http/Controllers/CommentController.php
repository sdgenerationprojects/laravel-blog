<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public  function  index($blog){
        $comments = Comment::with('user')->where('blog_id','=',$blog)->latest()->get();
        return response($comments,200);
    }
    public  function store(Request $request){
        $user = $request->user();

        $comment = new Comment;
        $comment->user_id = $user->id;
        $comment->blog_id = $request->blog_id;
        $comment->message = $request->message;

        $comment->save();

        return response($comment,200);
    }


}
