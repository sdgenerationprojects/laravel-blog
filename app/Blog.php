<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';

    protected $fillable = [
        'title', 'slug','detail','author_id',
    ];

    public function author(){
        return $this->belongsTo(User::class,'author_id','id')->select(['id','name']);
    }

    public function comment(){
        return $this->hasMany(Comment::class,'blog_id','id');
    }

}
